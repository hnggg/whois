
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

char *
lookup_host (const char *host)
{
  struct addrinfo hints, *res, *result;
  int errcode;
  char addrstr[100];
  void *ptr;

  memset (&hints, 0, sizeof (hints));
  hints.ai_family = AF_INET; /* Force v4 */
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags |= AI_CANONNAME;
  errcode = getaddrinfo (host, NULL, &hints, &result);
  if (errcode != 0){
      perror ("getaddrinfo");
  }

  res = result;
    int i = 0;
  while (res)
    {
      inet_ntop (res->ai_family, res->ai_addr->sa_data, addrstr, 100);

      switch (res->ai_family)
        {
        case AF_INET:
          ptr = &((struct sockaddr_in *) res->ai_addr)->sin_addr;
          break;
        case AF_INET6:
          ptr = &((struct sockaddr_in6 *) res->ai_addr)->sin6_addr;
          break;
        }
      inet_ntop (res->ai_family, ptr, addrstr, 100);
        if(i == 0){
            char * e = addrstr;
            return e;
        }
/*      res = res->ai_next;*/
        i = i + 1;
    }
  
  freeaddrinfo(result);

}
/*
int main (void)
{
  char inbuf[256];
  int len;
  do {
    bzero(inbuf, 256);
    printf("Type domain name:\n");
    fgets(inbuf, 256, stdin);
    len = strlen(inbuf);
    inbuf[len-1] = '\0';
    if(strlen(inbuf) > 0)
      printf("%s\n", lookup_host (inbuf));
    else
      return EXIT_SUCCESS;
  } while(1);
}
*/

char * hostname_to_ip(char * hostname)
{
    struct hostent *he;
    struct in_addr **addr_list;
    int i;
        
    if ( (he = gethostbyname( hostname ) ) == NULL) 
    {
        // get the host info
        herror("gethostbyname");
    }

    addr_list = (struct in_addr **) he->h_addr_list;
    
    for(i = 0; addr_list[i] != NULL; i++) 
    {
            return inet_ntoa(*addr_list[i]);

/*        printf("%s\n",addr_list[i]);*/
/*        strcpy(ip , inet_ntoa(*addr_list[i]) );*/
    }
    
}
