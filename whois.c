#include "dns.c"


#include <errno.h>
#include <netdb.h>
#include <ctype.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

char *trim(char *str){
    char *end;
    while(isspace((unsigned char)*str)) str++;
    if(*str == 0){
        return str;
    }
    end = str + strlen(str) - 1;
    while(end > str && isspace((unsigned char)*end)) end--;
    end[1] = '\0';
    return str;
}

char** str_split(char* a_str, const char a_delim){
    char** result    = 0;
    size_t count     = 0;
    char* tmp        = a_str;
    char* last_comma = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;
    while (*tmp){
        if (a_delim == *tmp){
            count++;
            last_comma = tmp;
        }
        tmp++;
    }
    count += last_comma < (a_str + strlen(a_str) - 1);
    count++;
    result = malloc(sizeof(char*) * count);
    if (result){
        size_t idx  = 0;
        char* token = strtok(a_str, delim);
        while (token){
            assert(idx < count);
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        assert(idx == count - 1);
        *(result + idx) = 0;
    }
    return result;
}


char * socketw(char * l, char * s, int d){
    char * el = l;
    char * ve = s;
    int b = 50000;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[50000] = {0};


    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0){
        printf("whois: socket connection failed \n");
    }
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(43);

    if(inet_pton(AF_INET, s, &serv_addr.sin_addr)<=0){
        printf("whois: invalid network address (this is a hard-wired code issue, you can't fix this)\n");
    }
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0){
        printf("whois: connection failed\n");
    }
    send(sock , strcat(el, "\r\n"), strlen(strcat(el, "\r\n")) , 0 );
    valread = read( sock , buffer, 50000);
    char * e = buffer;
    if(d == 1){
        printf("\nBufferedV : %i\n", valread);
        printf("BufferLen : %li\n", strlen(buffer));
        printf("ABuffer   : %i\n\n", b);
    }
    return e;
/*   return valread;*/
}

struct WFlags {
    int debug;
};

int main(int argc, char *argv[]){
    struct WFlags pl;
    int debugmode = 0;
    int i, x;
    int ch = 0;
    for (i=1; i<argc; i++) {
            if(strcmp(argv[i], "--debug") == 0 ){
                debugmode = 1;
            }
/*            if(strcmp(argv[i], "--hqt") == 0){
                pl.hqt = 1;
            }*/
/*                if (ch == 'A' || ch == 'a' || ch == 'e')
                    printf('Vowel\n');*/
    }

    if(strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0){
        printf("Usage: whois [QUERY] [FLAGS]\n");
        printf("-h, --help             Displays commands and help menu - This flag will ignore the query\n");
        printf("-c, --credits          Displays program credits - This flag will ignore the query\n");
        printf("    --debug            Enables additional printing of debug related variables.\n");
        exit(1);
    }

    char * domain = argv[1];
    char * sep = argv[3];
    char * tld = argv[2];
    tld = strcat(argv[3], argv[2]);
    char * ne[256]; /* this is for safety! */
    char *che;
    che = strtok(domain, ".");
    i = 0;
    while (che != NULL) {
        ne[i] = che;
        che = strtok(NULL, ".");
        i = i + 1;
    }
    size_t n = sizeof ne / sizeof *ne;

//    char * tld = ne[(i-1)];

    FILE* fp;
    fp = fopen("p/whois.txt", "r");
    if (fp == NULL) {
      perror("Failed: ");
      return 1;
    }

    char ** y;
    char buffer[256];
    char * we = "n/w";
    char * stringf = "/tmp/w-";

    FILE* fps = fopen("/tmp/wt.wtm", "w");
    if(fps == NULL){
        printf("whois: failed to write to /tmp/wt.wtm to prepare to escape a sequence.\n");
        exit(1);
    }
    fprintf(fps, "%s", "n/w");
    fclose(fps);
    while (fgets(buffer, 256 - 1, fp)){
        int d;
        buffer[strcspn(buffer, "\n")] = 0;
        y = str_split(buffer, ' ');
        if (y){
            int i;
            for(i = 0; *(y+i); i++){
                if(i == 0){
                    char * cd = trim(*(y+i));
                    if(strcmp(cd, tld) == 0){
                        d = 1;
                    }
                }
                if(i == 1 && d == 1){
                    we = *(y+i);
                    FILE* fps = fopen("/tmp/wt.wtm", "w");
                    if(fps == NULL){
                        printf("whois: failed to write to /tmp/wt.wtm to escape a sequence.\n");
                        exit(1);
                    }
                    fprintf(fps, "%s", we);
                    fclose(fps);
                    d = 0;
                }
                free(*(y + i));
            }
            free(y);
        }
    }
    fclose(fp);

    FILE* ptr = fopen("/tmp/wt.wtm", "r");
    if(ptr == NULL){
        printf("whois: couldn't open /tmp/wt.wtm to get out of a hole.\n");
    }

    char * wvalv;
    fscanf(ptr, "%s", wvalv);
    printf("%s\n", wvalv);
    fclose(ptr);


/*    if(strcmp(wvalv, "n/w") == 0){
        printf("whois: this tld isn't in our database. Update this program.\n");
        exit(1);
    }
*/

/*    wvalvi = htip(wvalv);
    printf("%s\n", wvalvi);
*/

/*    char * wvalvi = lookup_host(wvalv);*/

        printf("%s Sending query %s\n%s to %s (%s)\n", "%", domain, "%", lookup_host("whois.verisign-grs.com"), "whois.verisign-grs.com");

        printf("hi\n");
        printf("%s\n", socketw(strcat(domain, tld), "192.30.45.30", 0));
        printf("after whois\n");
/*    if(remove("/tmp/wt.wtm") != 0){
        printf("whois: can't delete temp. file.\n");
    }

*/
    return 0;
}
